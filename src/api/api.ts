import account from "@/api/accountAPI";
import school from "@/api/schoolAPI";
import tasks from "@/api/tasksApi";

export default { account: account, school: school, tasks: tasks }
