import API from './apiClient'

const tasks = {
    getTasksList: () => API.get('/tasks/api/tasks'),
    getTask: (taskId) => API.get(`/tasks/api/tasks/${taskId}`),
};

export default tasks;
