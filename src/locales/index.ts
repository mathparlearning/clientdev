export default {
    en: {
        "message": "asdfasfdasfd",
        "header": {
            "my_account": "My account",
            "apply_for_position": "Apply for position",
            "choose_profile": "Choose profile",
            "logout": "Logout"
        },
        "homePage": {
            "home_title": "Hello, I'm Mathpar!",
            "home_subtitle": "Your online math partner",
            "login_link": "I know right!",
            "registration_link": "I'm young padawan...",
        },
        "loginForm": {
            "login_form_title": "Login",
            "login_email_placeholder": "Email",
            "login_password_placeholder": "Password",
            "login_form_button": "Login",
            "forgot_password_link": "Forgot password?",
            "logout_message": "You are being logged out",
            "restore_password_title": "Restore password",
            "restore_email_placeholder": "Recovery email",
            "restore_button_label": "Send restoring link",
            "restore_success_message": "Restoring link was sent to your e-mail!",
            "forgot_password_title": "Set new password",
            "forgot_password_placeholder": "New password",
            "forgot_password_button": "Set new password",
            "forgot_password_message": "New password was successfully set! Go to "
        },
        "regForm": {
            "reg_form_title": "Registration",
            "reg_form_subtitle": "Create a new user account",
            "reg_email_placeholder": "Email",
            "reg_name_placeholder": "Name",
            "reg_password_placeholder": "Password",
            "reg_form_button": "Register me!",
        },
        "userCard": {
            "email_label": "Email",
            "username_label": "Username",
            "change_image_button": "Set this image"
        },
        "changePassword": {
            "change_password_title": "Change password",
            "new_password_placeholder": "Enter new password",
            "confirm_password_placeholder": "Confirm new password",
            "change_password_button": "Save new password"
        },
        "createSchool": {
            "create_school_title": "Create new school",
            "school_name_label": "Enter school name",
            "school_name_placeholder": "School name",
            "school_address_label": "Enter school address",
            "school_address_placeholder": "School address",
            "create_school_button": "Create school",
            "create_school_success_message": "School was created!",
            "create_school_error_message": "School wasn't created, please change your input values",
        },
        "invitationCardLoc": {
            "invitation_card_title": "Fill in the invitation card",
            "headmaster_email_label": "Enter the school headmaster's email",
            "headmaster_email_placeholder": "Email",
            "position_label": "Choose the position to apply",
            "position_placeholder": "Position is not chosen",
            "send_invitation_button": "Apply the request",
            "positions": [{"name": "Elementary school teacher"}, {"name": "High school teacher"}, {"name": "Head teacher"}]
        },
        "chooseProfileLoc": {
            "choose_profile_title": "Here are your profiles",
            "choose_profile_subtitle": "Choose profile from the list",
            "profile_created_label": "created",
            "no_profile_title": "You don't have active profiles yet",
            "no_profile_subtitle": "Let's do one of the following",
            "create_school_spoiler": "Add school",
            "apply_for_position_spoiler": "Apply for the position",
        },
        "createUserLoc": {
            "create_user_title": "Create user",
            "create_user_email_placeholder": "Email",
            "create_user_role_placeholder": "Role",
            "create_user_close_button": "Close",
            "create_user_create_button": "Create"
        },
        "schoolLoc": {
            "school_label": "School",
            "school_actions_label": "Actions",
            "school_headmaster_label": "Headmaster",
            "school_sidebar_users_link": "Users",
            "school_sidebar_groups_link": "Groups",
            "school_sidebar_edu_link": "Educational Plans",
            "school_sidebar_logout_link": "Logout",
            "school_sidebar_edit_button":"Edit",
            "school_sidebar_delete_button":"Delete",
            "school_user_headteacher_label":"Head teachers",
            "school_user_teacher_label":"Teachers",
            "school_user_student_label":"Students",
            "school_no_users_message":"There are no users yet",
            "school_add_user_button":"Add user",
            "school_groups_title":"Student Groups",
            "school_grades_label":"Grades",
            "school_groups_label":"Groups",
            "school_table_name_label":"Name",
            "school_table_teacher_label":"Teacher"
        },
        "taskLoc":{
            "task_answer_label":"Your answer",
            "answer_preview_label":"Answer preview",
            "task_answer_button":"Answer",
            "task_help_button":"Show me the result",
            "next_task_label":"Next task",
            "prev_task_label":"Previous task"
        },
        "sidebar": {
            "space": "Space",
            "constants": "Constants",
            "student": "Student",
            "plan": "Plan"
        }

    },
    ua: {
        "taskLoc":{
            "task_answer_label":"Ваша відповідь",
            "answer_preview_label":"Передогляд відповіді",
            "task_answer_button":"Відповісти",
            "task_help_button":"Подивитися розв'язання",
            "next_task_label":"До наступного",
            "prev_task_label":"До попереднього"
        },
        "header": {
            "my_account": "Мій акаунт",
            "apply_for_position": "Податись на посаду",
            "choose_profile": "Обрати профіль",
            "logout": "Вийти"
        },
        "homePage": {
            "home_title": "Привіт, я Матпар!",
            "home_subtitle": "Твій онлайн-партнер з математики",
            "login_link": "Увійти",
            "registration_link": "Зареєструватися",
        },
        "loginForm": {
            "login_form_title": "Вхід",
            "login_email_placeholder": "Електронна пошта",
            "login_password_placeholder": "Пароль",
            "login_form_button": "Увійти",
            "forgot_password_link": "Забули пароль?",
            "logout_message": "Ви вийшли з акаунта",
            "restore_password_title": "Відновити пароль",
            "restore_email_placeholder": "Електронна адреса для відновлення",
            "restore_button_label": "Надіслати посилання",
            "restore_success_message": "Посилання надіслано на вашу електронну адресу!",
            "forgot_password_title": "Встановити новий пароль",
            "forgot_password_placeholder": "Новий пароль",
            "forgot_password_button": "Встановити новий пароль",
            "forgot_password_message": "Пароль успішно змінено! Будь ласка, перейдіть на "
        },
        "regForm": {
            "reg_form_title": "Реєстрація",
            "reg_form_subtitle": "Створення нового акаунту користувача",
            "reg_email_placeholder": "Електронна пошта",
            "reg_name_placeholder": "Ім'я користувача",
            "reg_password_placeholder": "Пароль",
            "reg_form_button": "Зареєструвати мене",
        },
        "userCard": {
            "email_label": "Електронна пошта",
            "username_label": "Ім'я користувача",
            "change_image_button": "Застосувати зображення",
        },
        "changePassword": {
            "change_password_title": "Змінити пароль",
            "new_password_placeholder": "Введіть новий пароль",
            "confirm_password_placeholder": "Підтвердіть новий пароль",
            "change_password_button": "Зберегти новий пароль"
        },
        "createSchool": {
            "create_school_title": "Створити нову школу",
            "school_name_label": "Введіть назву школи",
            "school_name_placeholder": "Назва школи",
            "school_address_label": "Введіть адресу школи",
            "school_address_placeholder": "Адреса школи",
            "create_school_button": "Додати школу",
            "create_school_success_message": "Школу успішно створено!",
            "create_school_error_message": "Школу не створено, будь ласка, перевірте введені дані",
        },
        "invitationCardLoc": {
            "invitation_card_title": "Заповніть заявку",
            "headmaster_email_label": "Введіть електронну адресу директора школи",
            "headmaster_email_placeholder": "Електронна адреса",
            "position_label": "Оберіть позицію, на яку подаєтеся",
            "position_placeholder": "Позицію не обрано",
            "send_invitation_button": "Надіслати заявку",
            "positions": [{"name": "Вчитель молодших класів"}, {"name": "Вчитель старших класів"}, {"name": "Завідувач учбової частини"}]
        },
        "chooseProfileLoc": {
            "choose_profile_title": "Ось всі ваші профілі",
            "choose_profile_subtitle": "Оберіть профіль зі списку",
            "profile_created_label": "створено",
            "no_profile_title": "У вас поки немає активних профілів",
            "no_profile_subtitle": "Глянемо, що можна зробити",
            "create_school_spoiler": "Додати школу",
            "apply_for_position_spoiler": "Подати заявку",
        },
        "createUserLoc": {
            "create_user_title": "Створити користувача",
            "create_user_email_placeholder": "Електронна пошта",
            "create_user_role_placeholder": "Роль",
            "create_user_close_button": "Закрити",
            "create_user_create_button": "Створити"
        },
        "schoolLoc": {
            "school_label": "Школа",
            "school_actions_label": "Дії",
            "school_headmaster_label": "Директор",
            "school_sidebar_users_link": "Користувачі",
            "school_sidebar_groups_link": "Групи",
            "school_sidebar_edu_link": "Навчальні плани",
            "school_sidebar_logout_link": "Вихід",
            "school_sidebar_edit_button":"Редагувати",
            "school_sidebar_delete_button":"Видалити",
            "school_user_headteacher_label":"Завучі",
            "school_user_teacher_label":"Вчителі",
            "school_user_student_label":"Учні",
            "school_no_users_message":"Тут поки немає користувачів",
            "school_add_user_button":"Додати користувача",
            "school_groups_title":"Групи студентів",
            "school_grades_label":"Класи",
            "school_groups_label":"Групи",
            "school_table_name_label":"Назва",
            "school_table_teacher_label":"Вчитель"
        },
        "sidebar": {
            "space": "Простір",
            "constants": "Константи",
            "student": "Студент",
            "plan": "План"
        }
    }
}
