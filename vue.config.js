module.exports = {
    devServer: {
        proxy: 'http://mathpar.ukma.edu.ua/learning'
    },

    publicPath: process.env.NODE_ENV === 'production' ? '/learning/assets/' : '/',

    pluginOptions: {
      i18n: {
        locale: 'en',
        fallbackLocale: 'en',
        localeDir: 'locales',
        enableInSFC: true,
        enableBridge: false
      }
    }
}
